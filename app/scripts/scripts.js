$('.navbar').affix({
        offset: {
                 top: function () {
                      return (this.top = $(".navbar").offset().top);
                }
        }
});

$("ul.nav-bar li").click(function() {
    $('html, body').animate({
        scrollTop: $("#heroimg").offset().top
    }, 1);
});

$(".state1").click(function(){
	$(".home-container").show();
	$(".assortiment-content").hide();
	$(".vacature-content").hide();
	$(".news-content").hide();
	$(".sidebar").show();
	$(".actions-content").hide();
	$(".franchise-content").hide();
	$(".vestigingen-content").hide();
});

$(".state2").click(function(){
	$(".news-content").show();
	$(".home-container").hide();
	$(".actions-content").hide();
	$(".assortiment-content").hide();
	$(".vacature-content").hide();
	$(".sidebar").show();
	$(".franchise-content").hide();
	$(".vestigingen-content").hide();
});

$(".state3").click(function(){
	$(".vestigingen-content").show();
	$(".news-content").hide();
	$(".home-container").hide();
	$(".actions-content").hide();
	$(".assortiment-content").hide();
	$(".vacature-content").hide();
	$(".sidebar").hide();
	$(".franchise-content").hide();
});

$(".state4").click(function(){
	$(".actions-content").show();
	$(".home-container").hide();
	$(".assortiment-content").hide();
	$(".sidebar").hide();
	$(".vacature-content").hide();
	$(".news-content").hide();
	$(".franchise-content").hide();
	$(".vestigingen-content").hide();
});

$(".state5").click(function(){
	$(".assortiment-content").show();
	$(".actions-content").hide();
	$(".home-container").hide();
	$(".sidebar").hide();
	$(".assortiment").show();
	$(".vacature-content").hide();
	$(".news-content").hide();
	$(".franchise-content").hide();
	$(".vestigingen-content").hide();
});

$(".state6").click(function(){
	$(".franchise-content").show();
	$(".vacature-content").hide();
	$(".assortiment-content").hide();
	$(".actions-content").hide();
	$(".home-container").hide();
	$(".sidebar").hide();
	$(".news-content").hide();
	$(".vestigingen-content").hide();
});

$(".state7").click(function(){
	$(".vacature-content").show();
	$(".assortiment-content").hide();
	$(".actions-content").hide();
	$(".home-container").hide();
	$(".sidebar").show();
	$(".news-content").hide();
	$(".franchise-content").hide();
	$(".vestigingen-content").hide();
});
$(".new-location").click(function(){
	$(".vacature-content").hide();
	$(".assortiment-content").hide();
	$(".actions-content").hide();
	$(".home-container").hide();
	$(".sidebar").hide();
	$(".news-content").hide();
	$(".franchise-content").hide();
	$(".vestigingen-content").show();	
});
$(".news-burgers").click(function(){
	$(".news-content").hide();
	$(".assortiment-content").show();
	$(".assortiment-burgers").show();
	$(".sidebar").hide();
	
});
$(".news-salades").click(function(){
	$(".news-content").hide();
	$(".assortiment-content").show();
	$(".assortiment-salades").show();
	$(".sidebar").hide();
	
});

$("#burgers").click(function(){
	$(".assortiment-burgers").show();
	$(".assortiment").hide();
	$(".assortiment-broodjes").hide();
	$(".assortiment-schnitzels").hide();
	$(".assortiment-patat").hide();
	$(".assortiment-salades").hide();
	$(".assortiment-ijs").hide();

});
$("#broodjes").click(function(){
	$(".assortiment-broodjes").show();
	$(".assortiment").hide();
	$(".assortiment-schnitzels").hide();
	$(".assortiment-patat").hide();
	$(".assortiment-salades").hide();
	$(".assortiment-burgers").hide();
	$(".assortiment-ijs").hide();

});
$("#schnitzels").click(function(){
	$(".assortiment-schnitzels").show();
	$(".assortiment").hide();
	$(".assortiment-burgers").hide();
	$(".assortiment-patat").hide();
	$(".assortiment-salades").hide();
	$(".assortiment-broodjes").hide();
	$(".assortiment-ijs").hide();
});
$("#patat").click(function(){
	$(".assortiment-patat").show();
	$(".assortiment").hide();
	$(".assortiment-burgers").hide();
	$(".assortiment-schnitzels").hide();
	$(".assortiment-salades").hide();
	$(".assortiment-broodjes").hide();
	$(".assortiment-ijs").hide();

});
$("#salades").click(function(){
	$(".assortiment-salades").show();
	$(".assortiment").hide();
	$(".assortiment-broodjes").hide();
	$(".assortiment-burgers").hide();
	$(".assortiment-schnitzels").hide();
	$(".assortiment-patat").hide();
	$(".assortiment-ijs").hide();
});
$("#ijs").click(function(){
	$(".assortiment-ijs").show();
	$(".assortiment").hide();
	$(".assortiment-broodjes").hide();
	$(".assortiment-burgers").hide();
	$(".assortiment-schnitzels").hide();
	$(".assortiment-patat").hide();
	$(".assortiment-salades").hide();
});
$("#bbk").click(function(){
	$(".assortiment-content").hide();
	$(".actions-content").hide();
	$(".home-container").hide();
	$(".sidebar").hide();
	$(".assortiment").hide();
	$(".vacature-content").hide();
	$(".news-content").hide();
	$(".franchise-content").show();
	$(".vestigingen-content").hide();
})